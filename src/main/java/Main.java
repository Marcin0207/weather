import bizLogic.UserService;
import bizLogic.WeatherService;
import bizLogic.mappingModel.Weather;
import dataLayer.PreferencesDAO;
import dataLayer.UserDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import userInterface.*;
import util.HibernateUtil;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {



    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Witamy");
        System.out.println("Podaj login");
        String login = sc.nextLine();

        System.out.println("Wprowadź swoje tajne hasło");
        String pass = sc.nextLine();

        UserDAO userDAO = new UserDAO();
        PreferencesDAO preferencesDAO= new PreferencesDAO();
        UserService userService = new UserService(userDAO,preferencesDAO);
        userService.initializeUsers();


        WeatherService weatherService = new WeatherService(userService);


         ShowWeatherCommand showWeatherCommand = new ShowWeatherCommand(weatherService);
         ChangeCityCommand changeCityCommand = new ChangeCityCommand(userService);
         ChangeDaysCommand changeDaysCommand = new ChangeDaysCommand(userService) ;


        CommandRepository commandRepository = new CommandRepository();
        commandRepository.addCommand("ShowWeather", showWeatherCommand);
        commandRepository.addCommand("ChangeCity", changeCityCommand);
        commandRepository.addCommand("ChangeDays", changeDaysCommand);
        InputHandler handler = new CommandHandler(commandRepository);



        if(userService.login(login,pass)) {
            System.out.println("Witaj użytkowniku "+login);

            System.out.println("Wprowadź komende, dostępne polecenia: \n Pokaż pogode: ShowWeather \n Zmień miasto: ChangeCity \n " +
                    "Zmień liczbe dni: ChangeDays" );
            while(sc.hasNext()){

                String line = sc.nextLine();
                handler.handle(line);

                System.out.println("Wprowadź komende, dostępne polecenia: \n Pokaż pogode: ShowWeather \n Zmień miasto: ChangeCity \n " +
                        "Zmień liczbe dni: ChangeDays" );
            }
        }else System.out.println("błędne hasło");



    }



}
