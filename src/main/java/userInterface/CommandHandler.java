package userInterface;

public class CommandHandler implements InputHandler {

    private final CommandRepository commandRepository;

    public CommandHandler(CommandRepository commandRepository){
        this.commandRepository = commandRepository;
    }

    @Override
    public void handle(String input){

        Command command = null;
        if (commandRepository.getRepository().containsKey(input)) {
            command = commandRepository.getRepository().get(input);
            command.execute();
        }


    }


}
