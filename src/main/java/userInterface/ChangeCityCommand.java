package userInterface;

import bizLogic.UserService;

import java.util.Scanner;

public class ChangeCityCommand implements Command {
    UserService userService;

    public ChangeCityCommand(UserService userService){
        this.userService = userService;
    }


    @Override
    public void execute() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj miasto");
        String city = sc.nextLine();
        userService.changeDefaultCityOfCurrentUser(city);
        System.out.println("Ustawiono nowe miasto: "+ city);


    }
}
