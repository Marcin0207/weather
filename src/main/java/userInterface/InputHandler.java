package userInterface;

public interface InputHandler {
    void handle (String input);
}
