package userInterface;

import bizLogic.UserService;

import java.util.Scanner;

public class ChangeDaysCommand implements Command {
    UserService userService;

    public ChangeDaysCommand(UserService userService){
        this.userService = userService;
    }


    @Override
    public void execute() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj liczbe dni");
        int numberOfdays = sc.nextInt();
        userService.changeDefaultAmountOfDaysOfCurrentUser(numberOfdays);
        System.out.println("Ustawiono nową liczbe dni: "+numberOfdays );


    }
}