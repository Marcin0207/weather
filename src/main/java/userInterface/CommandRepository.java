package userInterface;

import java.util.HashMap;

public class CommandRepository {

    private HashMap<String, Command> repository;

    public CommandRepository(){
        repository = new HashMap<>();
    }

    public void addCommand(String name, Command command){
        repository.put(name,command);
    }
    public HashMap<String, Command> getRepository(){
        return repository;
    }


}
