package userInterface;

public interface Command {

    void execute();
}
