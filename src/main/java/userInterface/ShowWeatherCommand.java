package userInterface;

import bizLogic.WeatherService;

public class ShowWeatherCommand implements Command {

    WeatherService weatherService;

    public ShowWeatherCommand(WeatherService weatherService){
        this.weatherService = weatherService;
    }


    @Override
    public void execute() {
        System.out.println("Oto Twoja Pogoda");
        weatherService.showWeatherInfo();
    }
}
