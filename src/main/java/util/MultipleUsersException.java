package util;

public class MultipleUsersException extends RuntimeException{

    public MultipleUsersException(){
        super("wiele użytkowników o tej samej nazwie");
    }

}
