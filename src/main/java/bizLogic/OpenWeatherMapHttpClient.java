package bizLogic;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.swing.text.html.Option;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Optional;

public class OpenWeatherMapHttpClient {

    private final HttpClient httpClient;

    private static final String API_KEY = "5943d23ef349eb6be82b26c1c5f00f06";
    private static final String UNITS="METRIC";

    public OpenWeatherMapHttpClient(){
        this.httpClient = HttpClient.newHttpClient();
    }


    public OpenWeatherResponse getWeather(String city) throws IOException, InterruptedException {
        Optional<String> body =this.sendHTTPRequest(city);
        ObjectMapper objectMapper = new ObjectMapper() //tworzymy nowy obiekt mapper, coś co będzie nam konwertowało łańcuch znaków (JSON) na nasz obiekt OpenWeatherResponse, który wcześniej stworzyliśmy
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);// tym możemy wyłączyć rzucanie wyjątków w przypadku kiedy ObjectMapper natrafi w łańcuchu na pole, które nie ma odzwierciedlenia w klasie
        return objectMapper.readValue(body.get(), OpenWeatherResponse.class); // w końcu konwertujemy stringa na obiekt typu podanego w 2 argumencie (bez tego ObjectMapper nie wie na co zamienić tego Stringa)
    }


    private Optional<String> sendHTTPRequest(String city) throws IOException, InterruptedException{
        HttpRequest currentWeatherRequest = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("http://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid="+API_KEY+"&units="+UNITS))
                .build();
        String body = httpClient.send(currentWeatherRequest, HttpResponse.BodyHandlers.ofString())
                .body();
        if (body!=null & !body.isEmpty()){
            return Optional.of(body);
        }else return Optional.empty();

    }
}
