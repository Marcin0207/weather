package bizLogic;
import java.io.IOException;

public class WeatherService {

    private OpenWeatherMapHttpClient openWeatherMapHttpClient;
    private UserService userService;

    public WeatherService(UserService userService) {
        openWeatherMapHttpClient = new OpenWeatherMapHttpClient();
        this.userService = userService;
    }
    public void showWeatherInfo() {

        String city = userService.getCurrentUser().getUserPreferences().getDefaultCity();

        try {
            OpenWeatherResponse weatherResponse = openWeatherMapHttpClient.getWeather(city);
            System.out.println("Ogólnie: "+weatherResponse.getWeather().get(0).getDescription());
            System.out.println("Temperatura: "+weatherResponse.getMain().getTemp() + " °C");
            System.out.println("Temperatura odczuwalna: "+ weatherResponse.getMain().getFeels_like()+ " °C");
            System.out.println("Wilgotność: "+weatherResponse.getMain().getHumidity() +" %");
            System.out.println("Ciśnienie: "+weatherResponse.getMain().getPressure() +" hPa");
            System.out.println();



            //      System.out.println(openWeatherMapHttpClient.sendHTTPRequest("Warszawa"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
