package bizLogic;

import dataLayer.PreferencesDAO;
import dataLayer.entities.User;
import dataLayer.UserDAO;
import dataLayer.entities.UserPreferences;

import java.util.Optional;

public class UserService {

    private static final String USER_1_LOGIN = "user1";
    private static final String USER_2_LOGIN = "user2";
    private static final String USER_1_PASS = "password1";
    private static final String USER_2_PASS = "password2";
    private static final String USER_1_DEFAULT_CITY = "KRAKOW";
    private static final int USER_1_DEFAULT_NUMBER_OF_DAYS = 0;
    private static final String USER_2_DEFAULT_CITY = "WROCLAW";
    private static final int USER_2_DEFAULT_NUMBER_OF_DAYS = 0;
    private UserDAO userDAO;
    private PreferencesDAO preferencesDAO;
    private User currentUser;

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public UserService(UserDAO userDAO, PreferencesDAO preferencesDAO) {
        this.userDAO = userDAO;
        this.preferencesDAO = preferencesDAO;
    }

    public void initializeUsers() {
        createUserIfNotExist(USER_1_LOGIN, USER_1_PASS,USER_1_DEFAULT_CITY,USER_1_DEFAULT_NUMBER_OF_DAYS);
        createUserIfNotExist(USER_2_LOGIN, USER_2_PASS,USER_2_DEFAULT_CITY,USER_2_DEFAULT_NUMBER_OF_DAYS);
    }

    public void createUserIfNotExist(String login, String pass,String city, int days) {

        if (userDAO.findUser(login).isEmpty()) {
            userDAO.addUser(login, pass, city, days);
        }

    }

    public boolean login(String login, String pass) {

        String password = null;

        Optional<User> user = userDAO.findUser(login);
        if (user.isPresent()) {
            password = user.get().getPassword();
            if(password.equals(pass)){
                this.currentUser = user.get();
                return true;
            }else return false;
        }else {
            System.out.println("Nie znaleziono użytkownika");
            return false;
        }

    }

    public User findUser(String login){
        Optional<User> user = userDAO.findUser(login);

        if (user.isPresent()) {
            return user.get();
        }
        else return null;
    }

    public void changeDefaultCityOfCurrentUser(String city) {
        UserPreferences userPreferences = currentUser.getUserPreferences();
        userPreferences.setDefaultCity(city);
        preferencesDAO.updatePreferences(userPreferences);
    }
    public void changeDefaultAmountOfDaysOfCurrentUser(int days) {
        UserPreferences userPreferences = currentUser.getUserPreferences();
        userPreferences.setDefaultNumberOfDays(days);
        preferencesDAO.updatePreferences(userPreferences);

    }






}

