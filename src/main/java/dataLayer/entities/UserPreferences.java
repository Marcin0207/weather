package dataLayer.entities;

import javax.persistence.*;


@Entity
@Table(name = "User_preferences")
public class UserPreferences {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "id",columnDefinition = "BIGINT UNSIGNED")
        private Long id;

        @Column(name = "default_days_number", nullable = false, columnDefinition = "BIGINT UNSIGNED")
        private int defaultNumberOfDays;

        @Column(name = "default_city", nullable = false,columnDefinition = "VARCHAR(255)")
        private String defaultCity;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getDefaultNumberOfDays() {
        return defaultNumberOfDays;
    }

    public void setDefaultNumberOfDays(int defaultNumberOfDays) {
        this.defaultNumberOfDays = defaultNumberOfDays;
    }

    public String getDefaultCity() {
        return defaultCity;
    }

    public void setDefaultCity(String defaultCity) {
        this.defaultCity = defaultCity;
    }
}
