package dataLayer;

import dataLayer.entities.User;
import dataLayer.entities.UserPreferences;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import util.HibernateUtil;
import util.MultipleUsersException;

import java.util.List;
import java.util.Optional;

public class PreferencesDAO {
    SessionFactory sessionFactory;

    public PreferencesDAO(){
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }


    public void updatePreferences(UserPreferences userPreferences) {

        Session session = sessionFactory.openSession();
        session.beginTransaction();

        UserPreferences userPreferencesDb = session.get( UserPreferences.class,userPreferences.getId());

        userPreferencesDb.setDefaultCity(userPreferences.getDefaultCity());
        userPreferencesDb.setDefaultNumberOfDays(userPreferences.getDefaultNumberOfDays());
        session.save(userPreferencesDb);
        session.getTransaction().commit();
        session.close();
    }


}
