package dataLayer;

import dataLayer.entities.User;
import dataLayer.entities.UserPreferences;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import util.HibernateUtil;
import util.MultipleUsersException;

import java.util.List;
import java.util.Optional;

public class UserDAO {
    SessionFactory sessionFactory;

    public UserDAO(){
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    public Optional<User> findUser(String login) {

        Session session = sessionFactory.openSession();
        session.beginTransaction();

        Query query = session.createQuery("FROM User where login=:login", User.class);
        query.setParameter("login", login);
        List<User> userList = query.getResultList();
        if (userList.isEmpty()) {
            return Optional.empty();
        }else if (userList.size() > 1) {
            System.out.println("Istnieje kilku użytkowników o podanej nazwie");
            throw new MultipleUsersException();
        } else return Optional.of(userList.get(0));

    }

    public void addUser(String login, String pass, String city, int days) {

        Session session = sessionFactory.openSession();

        session.beginTransaction();

        User user1 = new User();
        user1.setLogin(login);
        user1.setPassword(pass);

        UserPreferences userPreferences = new UserPreferences();
        userPreferences.setDefaultCity(city);
        userPreferences.setDefaultNumberOfDays(days);
        user1.setUserPreferences(userPreferences);

        session.save(user1);
        session.getTransaction().commit();
        session.close();

    }


}
